var express = require('express');
var exec = require('exec');
var router = express.Router();


/* GET home page. */
router.get('/on', function(req, res, next) {
	//primer argumento: python archivo a ejecutar argumento 1 o 0
exec('python ../externalScript/led.py 1', function(err, out, code) {
  if (err instanceof Error)
    throw err;
  process.stderr.write(err);
  process.stdout.write(out);
  process.exit(code);
});
  
  res.write("Prendido");

});

router.get('/off', function(req, res, next) {
  
  exec('python ../externalScript/led.py 0', function(err, out, code) {
  if (err instanceof Error)
    throw err;
  process.stderr.write(err);
  process.stdout.write(out);
  process.exit(code);
});

res.write("apagado");

});


module.exports = router;